package com.kyjg.gatheringgameapi;

import com.kyjg.gatheringgameapi.entity.GameItemStat;
import com.kyjg.gatheringgameapi.model.OpenBoxPercentItem;
import com.kyjg.gatheringgameapi.repository.GameItemStatRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.LinkedList;
import java.util.List;

@SpringBootTest
class GatheringGameApiApplicationTests {
    @Autowired
    private GameItemStatRepository gameItemStatRepository;

    @Test
    void contextLoads() {
    }

    @Test
    void testPercent() {
        List<GameItemStat> gameItemStats = gameItemStatRepository.findAll();

        List<OpenBoxPercentItem> percentBar = new LinkedList<>();

        double oldPercent = 0;
        for (GameItemStat gameItemStat : gameItemStats) {
            OpenBoxPercentItem addItem = new OpenBoxPercentItem();

            addItem.setId(gameItemStat.getId());
            addItem.setPercentMin(oldPercent);
            addItem.setPercentMax(oldPercent + gameItemStat.getWoodBoxPercent());

            percentBar.add(addItem);

            oldPercent += gameItemStat.getWoodBoxPercent();
        }
        double result = (Math.random() * 100);

        long resultId = 0;
        for (OpenBoxPercentItem item : percentBar) {
            if (result >= item.getPercentMin() && result <= item.getPercentMax()) {
                resultId = item.getId();
                break;
            }
        }
        System.out.println(resultId);
    }

}
