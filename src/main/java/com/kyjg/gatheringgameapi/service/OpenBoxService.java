package com.kyjg.gatheringgameapi.service;

import com.kyjg.gatheringgameapi.entity.GameItemStat;
import com.kyjg.gatheringgameapi.entity.UserCollection;
import com.kyjg.gatheringgameapi.entity.UserStat;
import com.kyjg.gatheringgameapi.exception.CMissingDataException;
import com.kyjg.gatheringgameapi.exception.CNotEnoughMoneyException;
import com.kyjg.gatheringgameapi.model.*;
import com.kyjg.gatheringgameapi.repository.GameItemStatRepository;
import com.kyjg.gatheringgameapi.repository.UserCollectionRepository;
import com.kyjg.gatheringgameapi.repository.UserStatRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OpenBoxService {
    private final GameItemStatRepository gameItemStatRepository;
    private final UserCollectionRepository userCollectionRepository;
    private final UserStatRepository userStatRepository;

    long openBoxPay = 0L;

    private void init(boolean isWoodBox) {
        this.openBoxPay = isWoodBox ? 5000 : 15000;
    }

    public OpenBoxResultResponse getResult(boolean isWoodBox) {
        this.init(isWoodBox);

        boolean isEnoughMoney = this.isEnoughMoneyCheck(isWoodBox);

        if (!isEnoughMoney) throw new CNotEnoughMoneyException();

        UserStat userStat = userStatRepository.findById(1L).orElseThrow(CMissingDataException::new);
        userStat.minusMoneyCount(this.openBoxPay);

        userStatRepository.save(userStat);

        long openBoxResultId = this.openBoxResult(isWoodBox);

        boolean isNewItem = false;
        Optional<UserCollection> userCollection = userCollectionRepository.findByGameItemStat_Id(openBoxResultId);
        if (userCollection.isEmpty()) throw new CMissingDataException();
        UserCollection userCollectionData = userCollection.get();

        if (userCollectionData.getItemCount() == 0) isNewItem = true;

        userCollectionData.plusItemCount();
        userCollectionRepository.save(userCollectionData);

        OpenBoxResultResponse result = new OpenBoxResultResponse();
        result.setUserStatResponse(new UserStatResponse.UserStatResponseBuilder(userStat).build());
        result.setResultItem(new OpenBoxResultItem.OpenBoxResultItemBuilder(userCollectionData.getGameItemStat(), isNewItem).build());
        result.setUserCollectionItems(this.getMyItems());

        return result;
    }

    public List<UserCollectionItem> getMyItems() {
        List<UserCollection> originList = userCollectionRepository.findAllByIdGreaterThanEqualOrderByIdAsc(1L);

        List<UserCollectionItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new UserCollectionItem.UserCollectionItemBuilder(item).build()));

        return result;
    }

    private boolean isEnoughMoneyCheck(boolean isWoodBox) {
        UserStat userStat = userStatRepository.findById(1L).orElseThrow(CMissingDataException::new);

        boolean result = false;

        if (userStat.getMoneyCount() >= openBoxPay) result = true;

        return result;
    }


    private long openBoxResult(boolean isWoodBox) {

        List<GameItemStat> gameItemStats = gameItemStatRepository.findAll();

        List<OpenBoxPercentItem> percentBar = new LinkedList<>();

        double oldPercent = 0;
        for (GameItemStat gameItemStat : gameItemStats) {
            OpenBoxPercentItem addItem = new OpenBoxPercentItem();

            addItem.setId(gameItemStat.getId());
            addItem.setPercentMin(oldPercent);
            if (isWoodBox) {
                addItem.setPercentMax(oldPercent + gameItemStat.getWoodBoxPercent());
            } else
                addItem.setPercentMax(oldPercent + gameItemStat.getGoodBoxPercent());

            percentBar.add(addItem);

            if (isWoodBox) {
                oldPercent += gameItemStat.getWoodBoxPercent();
            } else
                oldPercent += gameItemStat.getGoodBoxPercent();
        }
        double result = (Math.random() * 100);

        long resultId = 0;
        for (OpenBoxPercentItem item : percentBar) {
            if (result >= item.getPercentMin() && result <= item.getPercentMax()) {
                resultId = item.getId();
                break;
            }
        }
        return resultId;
    }
}
