package com.kyjg.gatheringgameapi.entity;

import com.kyjg.gatheringgameapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UserCollection {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ApiModelProperty(notes = "게임 아이템 ID")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "gameItemStatId")
    private GameItemStat gameItemStat;
    @ApiModelProperty(notes = "보유 수량")
    @Column(nullable = false)
    private Integer itemCount;

    public void plusItemCount() {
        this.itemCount += 1;
    }
    public void resetItemCount() {
        this.itemCount = 0;
    }

    private UserCollection(UserCollectionBuilder builder) {
        this.gameItemStat = builder.gameItemStat;
        this.itemCount = builder.itemCount;

    }

    public static class UserCollectionBuilder implements CommonModelBuilder<UserCollection> {

        private final GameItemStat gameItemStat;
        private final Integer itemCount;

        public UserCollectionBuilder(GameItemStat gameItemStat) {
            this.gameItemStat = gameItemStat;
            this.itemCount = 0;
        }

        @Override
        public UserCollection build() {
            return new UserCollection(this);
        }
    }
}
