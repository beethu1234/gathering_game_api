package com.kyjg.gatheringgameapi.model;

import com.kyjg.gatheringgameapi.entity.GameItemStat;
import com.kyjg.gatheringgameapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class OpenBoxResultItem {
    @ApiModelProperty(notes = "등급 값")
    private String itemGrade;
    @ApiModelProperty(notes = "등급 명")
    private String itemGradeName;
    @ApiModelProperty(notes = "아이템 명")
    private String itemName;
    @ApiModelProperty(notes = "이미지 파일 명")
    private String imageFileName;
    @ApiModelProperty(notes = "새로운 광물 여부")
    private Boolean isNew;

    private OpenBoxResultItem(OpenBoxResultItemBuilder builder) {
        this.itemGrade = builder.itemGrade;
        this.itemGradeName = builder.itemGradeName;
        this.itemName = builder.itemName;
        this.imageFileName = builder.imageFileName;
        this.isNew = builder.isNew;
    }

    public static class OpenBoxResultItemBuilder implements CommonModelBuilder<OpenBoxResultItem> {

        private final String itemGrade;
        private final String itemGradeName;
        private final String itemName;
        private final String imageFileName;
        private final Boolean isNew;

        public OpenBoxResultItemBuilder(GameItemStat stat, boolean isNew) {
            this.itemGrade = stat.getItemGrade().toString();
            this.itemGradeName = stat.getItemGrade().getGradeName();
            this.itemName = stat.getItemName();
            this.imageFileName = stat.getImageFileName();
            this.isNew = isNew;
        }
        @Override
        public OpenBoxResultItem build() {
            return new OpenBoxResultItem(this);
        }
    }
}
