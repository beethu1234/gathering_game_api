package com.kyjg.gatheringgameapi.model;

import com.kyjg.gatheringgameapi.entity.UserStat;
import com.kyjg.gatheringgameapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UserStatResponse {
    private Long moneyCount;

    private UserStatResponse(UserStatResponseBuilder builder) {
        this.moneyCount = builder.moneyCount;
    }

    public static class UserStatResponseBuilder implements CommonModelBuilder<UserStatResponse> {

        private final Long moneyCount;

        public UserStatResponseBuilder(UserStat userStat) {
            this.moneyCount = userStat.getMoneyCount();
        }

        @Override
        public UserStatResponse build() {
            return new UserStatResponse(this);
        }
    }
}
