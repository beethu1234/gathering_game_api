package com.kyjg.gatheringgameapi.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class FirstConnectDataResponse {
    @ApiModelProperty(notes = "보유 금액 정보")
    private UserStatResponse userStatResponse;

    @ApiModelProperty(notes = "나의 컬렉션 목록")
    private List<UserCollectionItem> userCollectionItems;
}
