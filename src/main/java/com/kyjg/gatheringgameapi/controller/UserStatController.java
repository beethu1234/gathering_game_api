package com.kyjg.gatheringgameapi.controller;

import com.kyjg.gatheringgameapi.model.SingleResult;
import com.kyjg.gatheringgameapi.model.UserStatResponse;
import com.kyjg.gatheringgameapi.service.ResponseService;
import com.kyjg.gatheringgameapi.service.UserStatService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@Api(tags = "보유 금액 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/user-stat")
public class UserStatController {
    private final UserStatService userStatService;
    @ApiOperation(value = "클릭 시 돈 증가")
    @PutMapping("/money-plus")
    public SingleResult<UserStatResponse> putMoneyCountPlus() {

        return ResponseService.getSingleResult(userStatService.putMoneyCountPlus());
    }
}
