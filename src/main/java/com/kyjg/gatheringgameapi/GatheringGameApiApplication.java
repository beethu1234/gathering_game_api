package com.kyjg.gatheringgameapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GatheringGameApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatheringGameApiApplication.class, args);
    }

}
