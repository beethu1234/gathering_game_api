package com.kyjg.gatheringgameapi.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
