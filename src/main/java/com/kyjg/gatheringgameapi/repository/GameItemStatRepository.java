package com.kyjg.gatheringgameapi.repository;

import com.kyjg.gatheringgameapi.entity.GameItemStat;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GameItemStatRepository extends JpaRepository<GameItemStat, Long> {
}
