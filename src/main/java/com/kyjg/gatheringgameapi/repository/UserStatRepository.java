package com.kyjg.gatheringgameapi.repository;

import com.kyjg.gatheringgameapi.entity.UserStat;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserStatRepository extends JpaRepository<UserStat, Long> {
}
